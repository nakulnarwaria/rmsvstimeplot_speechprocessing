'''
driver.py
'''

from mydsp.audioframes import AudioFrames
from mydsp.rmsstream import RMSStream
import matplotlib.pyplot as plt


def driver():
    
    # Construct an RMS stream for the file "shaken.wav"
    # with the specified parameters.  Iterate to create a list
    # of frame intensities.  Plot them with time on the abcissa (x)
    # axis in seconds and RMS intensity on the ordinate (y) axis.
    
    # Be sure to listen to the audio to see if the intensity plot looks
    # correct.
    
    # Here's where you write lots of exciting stuff!
    
    # Might want to set a breakpoint here if your windows are closing on exit

    filename = "/Users/nakulnarwaria/PycharmProjects/SpeechProcessingAssignment/shaken.wav"
    audio_frames = AudioFrames(filename=filename, adv_ms=20, len_ms=10)
    rms_streams = RMSStream(audio_frames)

    time = []
    counter = 0
    time_instance = 0
    while(counter < len(rms_streams)):
        time.append(time_instance)
        time_instance += audio_frames.get_frameadv_ms()/1000
        counter += 1

    plt.figure()
    plt.plot(time, rms_streams.rms_stream)
    plt.title("RMS Energy")
    plt.xlabel("Seconds")
    plt.ylabel('dB rel.')
    plt.ion()
    plt.show()
    print()


if __name__ == '__main__':
    # If invoked as the main module, e.g. python driver.py, execute
    driver()