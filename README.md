## Abstract 
---

This project's objective is to plot RMS-energy vs time plot for a given audio file. Following implementation is coded in the project - 

The first class, AudioFrames, takes aconstructor containing a filename, and framing parameters giving the frameadvance and length in milliseconds. Iterating over the class will produce a seriesof frames. The second class is RMSStream and takes an instance ofAudioFrames as its constructor. Iterating over it produces RMS intensity valuesin dB.

## Sample Plot
---

A driver file is included in the project as well that reads the provided audio file�shaken.wav,� and produces a time-intensity plot of RMS energy using 20 ms frames advanced every 10 ms.

![RMS Energy Plot](https://user-images.githubusercontent.com/12014727/54393249-edb23b00-4666-11e9-8381-04b4bfd7ceb4.png)

## Credits
---

Code skeleton provided by **Professor Marie A. Roch** (San Diego State University)