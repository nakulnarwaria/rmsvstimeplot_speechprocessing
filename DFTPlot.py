from scipy.io import wavfile
from scipy import signal
import numpy as np
import matplotlib.pyplot as plt


Fs, x = wavfile.read('/Users/nakulnarwaria/Downloads/whistle1.wav')

x = x.T[0] #transpose x to match dimensions with window data
frame_N = len(x) # number of samples
frame_s = 0.020 # frame length

bins_Hz = np.arange(frame_N)/frame_N * Fs
window = signal.get_window("hamming", frame_N)
windowed_x = x * window
X = np.fft.fft(windowed_x)

# spectrum X is complex, convert to dB
magX = np.abs(X)
mag_dB = 20 * np.log10(magX)



plt.figure()
plt.plot(bins_Hz, mag_dB)
plt.title("discrete Fourier transform")
plt.xlabel("Hz")
plt.ylabel('dB rel.')
plt.ion()
plt.show()
print()